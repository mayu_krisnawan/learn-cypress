import React from 'react'

class PhotoCard extends React.Component {
  render() {
    const { download_url: url, author } = this.props.photo
    return (<div className='photo-card'>
      <div className='crop'>
        <img src={url} alt={author}/>
      </div>
      <div className='caption'>
        {author}
      </div>
    </div>)
  }
}

export default PhotoCard
