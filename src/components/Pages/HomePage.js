import React from 'react';
import { Container } from 'reactstrap';
import PhotoCard from '../PhotoCard';

class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoadingPhotos: false,
      photos: [],
    }

    this.loadPhotos = () => {
      this.setState({ isLoadingPhotos: true })
      fetch('https://picsum.photos/v2/list?page=1&limit=30')
        .then(response => response.json())
        .then(photos => {
          this.setState({ isLoadingPhotos: false, photos })
        })
    }
  }

  componentDidMount() {
    this.loadPhotos()
  }

  render() {
    const { isLoadingPhotos, photos } = this.state

    return (<Container>
      {isLoadingPhotos && (<div className='loading-container'>
        <i>Loading photos...</i>
      </div>)}
      {!isLoadingPhotos && <div className='d-flex flex-wrap justify-content-center'>
        {photos.map((photo, index) => {
          return <PhotoCard key={index} photo={photo}/>
        })}
      </div>}
    </Container>)
  }
}

export default HomePage
