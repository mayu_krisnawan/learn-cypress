import React from 'react'
import {
  Button,
  Container,
  Form, FormGroup,
  Label, Input
} from 'reactstrap'

class ContactUsPage extends React.Component {
  constructor(props) {
    super(props)
    this.emailRef = React.createRef()
  }

  onSubmit(e) {
    e.preventDefault()
    const confirmed = window.confirm('Are you sure your data is correct?')
    if (confirmed) {
      alert('Data submitted')
      const email = this.emailRef.current.value
      window.open(`mailto:${email}`, 'redirect')
    }
  }

  render() {
    return (<Container>
      <h1>Contact Us</h1>
      <hr/>
      <Form onSubmit={this.onSubmit.bind(this)}>
        <FormGroup>
          <Label>Name</Label>
          <Input type="text" name="name"/>
        </FormGroup>
        <FormGroup>
          <Label>Email</Label>
          <Input innerRef={this.emailRef} type="email" name="email"/>
        </FormGroup>
        <FormGroup>
          <Label>Note</Label>
          <Input type="textarea" name="note"/>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" name="is_send_copy"/>{' '}
            Send copy of response to my email
          </Label>
        </FormGroup>
        <FormGroup style={{ paddingTop: 20 }}>
          <Button color="primary" type="submit" id="submitButton">Submit</Button>
        </FormGroup>
      </Form>
    </Container>)
  }
}

export default ContactUsPage;
