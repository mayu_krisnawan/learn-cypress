import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import { Navbar, NavbarBrand, Nav, NavItem } from 'reactstrap'
import HomePage from './Pages/HomePage'
import ContactUsPage from './Pages/ContactUsPage'

function App() {
  return (<Router>
    <Navbar color="primary" dark expand="md">
      <NavbarBrand href="/">Learn Cypress</NavbarBrand>
      <Nav>
        <NavItem>
          <Link className="nav-link text-light" to="/">Home</Link>
        </NavItem>
        <NavItem>
          <Link className="nav-link text-light" to="/contact-us">Contact Us</Link>
        </NavItem>
      </Nav>
    </Navbar>
    <div>
      <Switch>
        <Route exact path="/">
          <HomePage/>
        </Route>
        <Route exact path="/contact-us">
          <ContactUsPage/>
        </Route>
      </Switch>
    </div>
  </Router>);
}

export default App;
