describe('Home Page', () => {
  beforeEach(function() {
    const host = Cypress.env('APP_HOST')
    cy.server()
    cy.route('GET', 'https://picsum.photos/**').as('getPhotos')
    cy.visit(host, {
      onBeforeLoad: (win) => {
        win.fetch = null
      }
    })
  })

  it('should load photo list', function() {
    cy.wait('@getPhotos').should('have', 'status', 200)
  })

  it('should display valid photos', function() {
    cy.wait('@getPhotos').should(xhr => {
      return new Response(xhr.response.body).text().then(text => {
        this.photos = JSON.parse(text)
      })
    })
    cy.get('.photo-card').should('have.length', 30).each(($el, index) => {
      const caption = $el.find('.caption').eq(0).text()
      expect(caption).to.eq(this.photos[index].author)
      const url = $el.find('img').eq(0).attr('src')
      expect(url).to.eq(this.photos[index].download_url)
    })
  })
})
