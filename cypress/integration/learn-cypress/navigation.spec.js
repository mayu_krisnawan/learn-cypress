describe('Navigation', () => {
  it('should navigate page correctly', () => {
    const host = Cypress.env('APP_HOST')
    cy.visit(host)
    cy.get('.nav-link').contains('Contact Us').click()
    cy.location().should(location => {
      expect(location.pathname).to.eq('/contact-us')
    })
    cy.get('.nav-link').contains('Home').click()
    cy.location().should(location => {
      expect(location.pathname).to.eq('/')
    })
  })
})
