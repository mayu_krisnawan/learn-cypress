describe('Contact Us', () => {
  beforeEach(function() {
    cy.fixture('contact.json').then(contact => {
      this.contact = contact
    })
    const host = Cypress.env('APP_HOST')
    cy.visit(`${host}/contact-us`, {
      onBeforeLoad(win) {
        cy.stub(win, 'open').as('windowOpen')
      }
    })
  })

  it('should respond to input', function() {
    const { name, email, note, is_send_copy } = this.contact
    cy.fillInput({ name, email, note })
    cy.get('input[name="name"]').should('have.prop', 'value', name)
    cy.get('input[name="email"]').should('have.prop', 'value', email)
    cy.get('textarea[name="note"]').should('have.prop', 'value', note)
    cy.get('input[name="is_send_copy"]').check().should('have.prop', 'checked', is_send_copy)

    cy.on('window:confirm', message => {
      expect(message).to.eq('Are you sure your data is correct?')
      return true
    })
    cy.on('window:alert', message => {
      expect(message).to.eq('Data submitted')
    })
    cy.get('#submitButton').click()
    cy.get('@windowOpen').should('be.calledWith', `mailto:${email}`)
  })
})
